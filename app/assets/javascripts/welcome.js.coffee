# Demonstrate the combined use of CoffeeScript, Turbolinks, jQuery and Moment.
# Update the current time reading on every second.

updateCurrentTime = ->
  $('#current-time').text moment().format('MMMM Do YYYY, h:mm:ss a')

setupUpdates = ->
  updateCurrentTime()
  setInterval updateCurrentTime, 1000

$(setupUpdates)
$(document).on 'page:load', setupUpdates
